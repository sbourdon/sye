export function getDatasFromElement(key, el, value) {
  const obj = {
    type: key,
    dom: el,
  };
  switch (key) {
    case 'text':
      obj.value = el.textContent;
      break;
    case 'html':
      obj.value = el.innerHTML;
      break;
    case 'attr':
      obj.value = el.getAttribute(`data-${value}`);
      break;
    default:
      break;
  }
  return obj;
}

export function updateDomFromDatas(obj, value, newValue) {
  const newObj = obj[value];
  switch (newObj.type) {
    case 'text':
      newObj.dom.textContent = newValue;
      break;
    case 'html':
      newObj.dom.innerHTML = newValue;
      break;
    case 'attr':
      newObj.dom.setAttribute(`data-${value}`, newValue);
      break;
    default:
      break;
  }
  return newObj;
}
