/*
 * Component
 *
 * The component master class.
 *
 * @module component
 *
 */

import * as utils from './componentUtils';
import * as logger from './logger';

export default class Component {
  constructor(selector) {
    this.root = selector;
    this.root.__syecomponent__ = this;
    this._name = this.root.getAttribute('data-component');
    this.datas = {};
    this._datas = {};
    this._refs = {};
    this.devTools = true;
    this.eventsInitialized = false;
  }

  set name(value) {
    this._name = value;
  }

  get name() {
    return this._name;
  }

  load() {
    // Get initialization objet and build the component
    this.configObj = this.initialize();
    this.isPrefixed = this.configObj.prefixed || false;
    this.configObj.refs && this.handleReferences(this.configObj.refs);
    this.configObj.datas && this.handleDataBindings(this.configObj.datas);
    this.configObj.events && this.handleEventRegistration(this.configObj.events, 'load');
    this.beforeMount();
    this.onMount();
    this.afterMount();
  }

  unload() {
    this.configObj.events && this.handleEventRegistration(this.configObj.events, 'unload');
  }

  reload() {
    this.configObj.events && this.handleEventRegistration(this.configObj.events, 'load');
  }

  initialize() {
    // here to rewritten on component
  }

  beforeMount() {
    // here to rewritten on component
  }

  onMount() {
    // here to rewritten on component
  }

  afterMount() {
    // here to rewritten on component
  }

  beforeUpdate() {
    // here to rewritten on component
  }

  onUpdate(dataObj, value, newValue) {
    utils.updateDomFromDatas(dataObj, value, newValue);
  }

  afterUpdate() {
    // here to rewritten on component
  }

  handleReferences(refs) {
    refs.forEach(ref => {
      const nodeList = this.isPrefixed
        ? this.root.querySelectorAll(`[data-ref="${this.name}:${ref}"]`)
        : this.root.querySelectorAll(`[data-ref="${ref}"]`);
      const name = ref.toString();
      if (nodeList.length === 0) {
        this.devTools && logger.error([`${this.name} component - Can't found any ${ref} reference`]);
        return;
      }
      // if ref is unique, register the dom node else register the node list
      nodeList.length === 1 ? ([this._refs[name]] = nodeList) : (this._refs[name] = nodeList);
    });
    this.devTools && logger.success([`${this.name} component - Those refs have been added :`], [this._refs]);
  }

  handleDataBindings(datas) {
    const _this = this;
    Object.keys(datas).forEach(key => {
      if (key === 'text' || key === 'html' || key === 'attr' || key === 'form') {
        datas[key].forEach(value => {
          let el = this.isPrefixed
            ? _this.root.querySelector(`[data-bind="${this.name}:${value}"]`)
            : _this.root.querySelector(`[data-bind="${value}"]`);
          // if we don't find the element on child nodes we try to find it on root element
          if (!el && _this.root.hasAttribute('data-bind')) {
            el = _this.root;
          }
          if (el) {
            _this._datas[value] = utils.getDatasFromElement(key, el, value);
            Object.defineProperty(this.datas, value, {
              get() {
                return _this._datas[value].value;
              },
              set(newValue) {
                _this.beforeUpdate();
                // Set the new value on data object then trigger update method
                _this._datas[value].value = newValue;
                _this.onUpdate(_this._datas, value, newValue);
                _this.afterUpdate();
              },
            });
          }
        });
      }
    });
    this.devTools && logger.success([`${this.name} component - Those datas have been added :`], [this._datas]);
  }

  handleEventRegistration(events, type) {
    if (type === 'load' && !this.eventsInitialized) {
      Object.keys(events).forEach(event => {
        const triggers = this.isPrefixed
          ? this.root.querySelectorAll(`[data-action="${this.name}:${event}"]`)
          : this.root.querySelectorAll(`[data-action="${event}"]`);
        triggers.forEach(trigger => {
          /* eslint no-param-reassign: ["error", { "props": false }] */
          trigger.addEventListener(
            events[event],
            (trigger.fn = e => {
              this[event](e);
            })
          );
        });
        this.eventsInitialized = true;
      });
    }

    if (type === 'unload' && this.eventsInitialized) {
      Object.keys(events).forEach(event => {
        const triggers = this.isPrefixed
          ? this.root.querySelectorAll(`[data-action="${this.name}:${event}"]`)
          : this.root.querySelectorAll(`[data-action="${event}"]`);

        triggers.forEach(trigger => {
          trigger.removeEventListener(events[event], trigger.fn);
        });
        this.eventsInitialized = false;
      });
    }
  }
}
