/* eslint-disable no-shadow */
import * as logger from './logger';
/**
 * Event Bus
 *
 * A pub /sub like pattern to help communication between modules
 * dispatching events.
 *
 * @module utils/eventbus
 *
 */

export default class EventBus {
  constructor() {
    this.listeners = new Map();
    this.devTools = false;
  }

  /**
   * @function
   * @description add a listener to EventBus
   * @param {string} event the event name
   * @param {function} callback function reference added to listeners list
   */
  on(event, callback) {
    if (!this.listeners.has(event)) {
      this.listeners.set(event, []);
    }
    this.listeners.get(event).push(callback);
    this.devTools && logger.success([`${event} has a new listener : ${callback}`]);
  }

  /**
   * @function
   * @description remove a listener from a listerner list
   * @param {string} event the event name
   * @param {function} callback function reference to the ready to remove listener
   */
  off(event, callback) {
    const listeners = this.listeners.get(event);
    let index;

    if (listeners && listeners.length) {
      // Return listener corresponding index
      index = listeners.reduce((i, listener, index) => {
        // @ToBeImproved : remove toString from comparison and add context/scope
        if (typeof listener === 'function' && listener.toString === callback.toString) {
          // eslint-disable-next-line no-param-reassign
          i = index;
        }
        return i;
      }, -1);

      // Remove the listener
      if (index > -1) {
        listeners.splice(index, 1);
        this.listeners.set(event, listeners);
        return true;
      }
    }
    return false;
  }

  /**
   * @function
   * @description dispatch an event
   * @param {string} event the event name
   * @param {object} args arguments passed to listener callback function
   */
  emit(event, ...args) {
    const listeners = this.listeners.get(event);

    if (listeners && listeners.length) {
      // @ToBeImproved : add a queue sytem sorted by priority
      listeners.forEach(listener => {
        listener(...args);
        this.devTools && logger.success([`${event} have been dispatched to : ${listener}`]);
      });
      return true;
    }
    return false;
  }
}
