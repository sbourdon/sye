/*
 * Container
 *
 * The container master clas.
 *
 * @module container
 *
 */

export default class Container {
  constructor(selector) {
    this.root = selector;
    this.root.__syecontainer__ = this;
    this.devTools = false;
  }

  load() {
    this.beforeMount();
    this.onMount();
    this.afterMount();
  }

  beforeMount() {
    // here to rewritten on component
  }

  onMount() {
    // here to rewritten on component
  }

  afterMount() {
    // here to rewritten on component
  }
}
