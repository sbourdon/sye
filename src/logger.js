/* eslint-disable no-console */
const colors = {
  default: 'black',
  success: 'green',
  warning: 'orange',
  error: 'red',
};

export function print(color, msgs) {
  msgs.forEach(msg => {
    typeof msg[0] === 'object'
      ? console.table(msg[0], { colors: true, depth: 1 })
      : console.log(`%c${msg[0]}`, `color:${color}`);
  });
}

export function log(...msgs) {
  print(colors.default, msgs);
}

export function success(...msgs) {
  print(colors.success, msgs);
}

export function warn(...msgs) {
  print(colors.warning, msgs);
}

export function error(...msgs) {
  print(colors.error, msgs);
}
