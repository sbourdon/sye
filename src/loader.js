const Loader = {
  loadContainers(containers) {
    Object.keys(containers).forEach(container => {
      // get dom instance
      const instances = document.querySelectorAll(`[data-container="${container}"]`);
      instances.forEach(instance => {
        const containerInstance = new containers[container](instance);
        if (containerInstance.root.__syecontainer__ !== 'undefined') {
          containerInstance.load();
        }
      });
    });
  },
  loadComponents(components, container) {
    Object.keys(components).forEach(component => {
      // get dom instance
      const instances = container.root.querySelectorAll(`[data-component="${component}"]`);
      instances.forEach(instance => {
        const componentInstance = new components[component](instance);
        if (componentInstance.root.__syecomponent__ !== 'undefined') {
          componentInstance.load();
        }
      });
    });
  },
  unloadComponents(components, container) {
    Object.keys(components).forEach(component => {
      // get dom instance
      const instances = container.root.querySelectorAll(`[data-component="${component}"]`);
      instances.forEach(instance => {
        const componentInstance = instance;
        if (componentInstance.root.__syecomponent__ === componentInstance) {
          componentInstance.__syecomponent = null;
        }
      });
    });
  },
};

export default Loader;
